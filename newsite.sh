#!/bin/bash
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root"
   exit 1
fi

printf "Enter Username: "
read user

while [[ -z "$user" ]]
do
  printf "Username can't be blank\nEnter Username: "
  read user
done

printf "\nEnter Password: "
read -s password

while [[ -z "$password" ]]
do
  printf "Password can't be blank\nEnter Password: "
  read -s password
done

printf "\nConfirm Password: "
read -s confirm

if [ "$confirm" != "$password" ]
then
    echo "Passwords don't maatch. Exiting"
    exit 1
fi

printf "\n\nEnter Domain: "
read domain

while [[ -z "$domain" ]]
do
  printf "Domain can't be blank\nEnter Domain: "
  read domain
done

 
echo "\ncreating user"
pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)
useradd -m -p $pass $user
usermod -G sftp-users $user

echo "Set User to SFTP Access"
cp /etc/ssh/sshd_config /etc/ssh/sshd_config.bak
sed -i -e "s/AllowUsers/AllowUsers $user/g" /etc/ssh/sshd_config

echo "Set lsync user sync"
cp /etc/lsyncd.conf /etc/lsyncd.conf.bak
sed -i -e "s/\"xscomau7\"/\"xscomau7\",\"$user\"/g" /etc/lsyncd.conf
 
echo "creating html directory"
mkdir /home/$user/public_html
chown -R root /home/$user
chgrp -R root /home/$user
chown -R $user /home/$user/public_html
chgrp -R $user /home/$user/public_html
chmod 755 /home/$user
chmod 755 /home/$user/public_html

echo "Creating the vhost config file"
cat <<EOF > /etc/httpd/conf.d/$user.conf
<VirtualHost *:80>
    suPHP_Engine on
    AddHandler x-httpd-php .php .php3 .php4 .php5
    suPHP_AddHandler x-httpd-php
    suPHP_UserGroup $user $user
    ServerAdmin web@grantbroadcasters.com.au
    DocumentRoot /home/$user/public_html
    ServerName $domain
    ServerAlias www.$domain admin.$domain
    <Directory "/home/$user/public_html">
        AllowOverride All
        Order Allow,Deny
        Allow from All
    </Directory>
#    ErrorLog logs/$domain-error_log
#    CustomLog logs/$domain-access_log common
</VirtualHost>
EOF

echo "restarting apache"
service httpd restart

echo "restarting ssh"
service sshd restart

echo "restarting lsyncd"
service lsyncd restart